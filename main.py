import gc

import time

file = open("gray.bmp", mode="rb")

file.seek(10)  # scan to where the starting address is stored
location = file.read(4)  # read the offset location (in little-endian integer bytes)
offset = int.from_bytes(location, "little")

# get the width and height from the header
file.seek(18)
width = int.from_bytes(file.read(4), "little")
height = int.from_bytes(file.read(4), "little")

file.seek(offset)

bytes_per_pixel = 3
bytes_per_line = bytes_per_pixel * width
cycles = 0

def read_line(f, offset, row):
    global cycles
    line = f.read(bytes_per_line)  # read the bitmap data

    remainder = bytes_per_line % 4
    if remainder != 0:
        f.seek(remainder, 1)

    # Reset to beginning of bitmap data if we reached the end
    if len(line) == 0:
        f.seek(offset)
        f.read(bytes_per_line)

        cycles += 1

    i = 0
    k = 0
    for x in range(width):
        if (i + 2 >= len(line)):
            break
        row[k] = ((line[i] << 0) + (line[i+1] << 8) + (line[i+2] << 16))
        i += bytes_per_pixel
        k += 1

    return row

OFF = 0x000000

start = time.monotonic()
MINUTES = 60

buf = [0] * width
finished = False

def elapsed():
    return time.monotonic() - start

print(f"offset={offset}, width={width}, height={height}")

while cycles == 0:
    read_line(file, offset, buf)

    for i in range(len(buf)):
        print("%06x " % buf[i], end="")
    
    print("", end="\n")
    
    
