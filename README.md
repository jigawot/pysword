# PySword

This project is for the Adafruit ItsyBitsy M0 Express running code for Jonathan Hoffman's sculpture. It powers a custom-built string of LEDs that are embedded into a sword.

## Usage

Copy your 20-pixel-wide BMP image to `sword.bmp` at the root of the drive (`CIRCUITPI`). Power it on and watch it go!

If you want to include a power-on animation, use a `poweron.bmp` file, which will animate once and then transition into `sword.bmp`. This file is optional.
