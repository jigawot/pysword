line = 0
bytes_per_line = 8
with open("gray.bmp", mode="rb") as file:
    while True:
        buf = file.read(bytes_per_line)
        
        if not buf:
            break

        print("%04x (%05d): " % (line, line), end="")

        j = 0
        for i in buf:
            print("%02x " % i, end="")
        
            if j == 3:
                print(" ", end="")
                j = 0
            else:
                j += 1
            

        print(end = "\n")

        line += bytes_per_line

